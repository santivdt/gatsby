require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "React tutorial",
    menuLinks: [
        {
            name: 'Home',
            link: '/'
        },
        {
            name: 'About',
            link: '/about'
        },
        {
            name: 'Instagram',
            link: '/instagram'
        },
        {
            name: 'Wordpress',
            link: '/wordpress'
        }
      ],
  },
plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-gatsby-cloud",
    "gatsby-plugin-image",
    "gatsby-image",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
        resolve: "gatsby-source-filesystem",
        options: {
            name: `blog`,
            path: `${__dirname}/blog`,
        }
    },
    "gatsby-plugin-mdx",
    "gatsby-theme-material-ui",
    {
        resolve: `gatsby-source-instagram-all`,
        options: {
            access_token: process.env.INSTAGRAM_TOKEN
        }
    },
    "gatsby-plugin-react-helmet",
    {
        resolve: `gatsby-source-wordpress`,
        options: {
            url: `https://campaigntool.nl/anticonceptie/graphql`,
            includedRoutes: [
                '**/posts',
                '**/tags',
                '**/categories',
                '**/pages'
            ]
        },
    },
  ],

};
