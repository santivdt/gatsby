import * as React from 'react'
import Box from '@material-ui/core/Box'
import {footer} from './footer.module.scss'

const Footer = () => {
    return (
        <Box display="flex" justifyContent="center" className={footer}>
                Copyright 2021 - SvdT JWZ
        </Box>
    )
}

export default Footer