import * as React from 'react'

const Greeting = ({name, children}) => {
    return (
        <p>
            Hi {name}
            {children}
        </p>
    )
}

export default Greeting
