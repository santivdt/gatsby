import * as React from 'react'
import {image} from './instagram.module.scss'

const InstaImage = ({permalink, media_url}) => {
    return (
        <div>
            <a href={permalink} target="new">
                <img className={image}
                     src={media_url}
                     alt="foto"
                />
            </a>
        </div>    )
}

export default InstaImage