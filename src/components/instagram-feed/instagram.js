import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Box from '@material-ui/core/Box'
import _get from "lodash/get"
import {container_image} from './instagram.module.scss'
import InstaVideo from './video.js'
import InstaImage from './image.js'
import ReadMore from "../read_more/read_more.js";

const Insta = () => {

    const data = useStaticQuery(graphql`
        query {
          allInstagramContent {
            edges {
              node {
                id
                media_type
                caption
                media_url
                permalink
                timestamp
                username
              }
            }
          }
        }
    `)

    let arrayOfInstaImages = _get(data, 'allInstagramContent.edges')

    return (
        <Box display="flex" flexDirection="column">
                {arrayOfInstaImages.map((item, i) => {
                    return (
                        <div key={i} className={container_image}>
                            {item.node.media_type === "VIDEO" ? (
                                <InstaVideo permalink={item.node.permalink} media_url={item.node.media_url}/>
                            ) : (
                                <InstaImage permalink={item.node.permalink} media_url={item.node.media_url}/>
                                    )}
                            <p> <ReadMore>{item.node.caption}</ReadMore> </p>
                        </div>
                    )
                })}
        </Box>
    )
}

export default Insta