import * as React from 'react'
import {image} from './instagram.module.scss'

const InstaVideo = ({permalink, media_url}) => {
    return (
        <div>
            <a href={permalink} target="new">
                <video className={image}>
                <source src={media_url} type="video/mp4"/>
                </video>
            </a>
        </div>    )
}

export default InstaVideo