import * as React from 'react'
import {
    container
} from './layout.module.scss'
import { useStaticQuery, graphql } from 'gatsby'
import Box from '@material-ui/core/Box'
import Footer from './footer/footer.js'
import Menu from './menu/menu.js'
import { Helmet } from "react-helmet"

const Layout = ({pageTitle, children}) => {

    const data = useStaticQuery(graphql`
      query {
      site {
        siteMetadata {
          title
        }
      }
    }
    `)

    return (
        <>
            <Helmet>
                <title>{data.site.siteMetadata.title}</title>
                <meta name="description" content={data.site.siteMetadata.description} />
            </Helmet>
            <Box className={container} display="flex" flexDirection="column" justifyContent="space-between">
                <Box display="flex" justifyContent="flex-start" flexDirection="column">
                    <title>{pageTitle} | {data.site.siteMetadata.title}</title>
                    <Menu/>
                    <main>
                        <h1>{pageTitle}</h1>
                        {children}
                    </main>
                </Box>
                <Footer/>
            </Box>
        </>
    )
}

export default Layout