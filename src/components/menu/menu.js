import * as React from 'react'
import Box from '@material-ui/core/Box'
import {navLinkItem, navLinks, navLinkText, siteTitle} from "./menu.module.scss";
import {graphql, Link, useStaticQuery} from "gatsby";

const Menu = () => {

    const data = useStaticQuery(graphql`
      query {
          site {
            id
            siteMetadata {
              title
              menuLinks {
                link
                name
              }
            }
          }
        }
    `)


    return (
        <Box display="flex" flexDirection="row" alignItems="center">
            <header className={siteTitle}>{data.site.siteMetadata.title}</header>
            <nav>
                <ul className={navLinks}>
                        {Object.keys(data.site.siteMetadata.menuLinks).map(function(keyName, keyIndex) {
                            return (
                                <li key={keyName} className={navLinkItem}>
                                    <Link to={data.site.siteMetadata.menuLinks[keyName].link} className={navLinkText}>
                                        {data.site.siteMetadata.menuLinks[keyName].name}
                                    </Link>

                                </li>
                        )
                        })}
                </ul>
            </nav>
        </Box>
    )
}

export default Menu