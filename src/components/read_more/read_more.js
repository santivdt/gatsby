import React, { useState } from 'react'
import './read_more_module.scss'

const ReadMore = ({ children }) => {
    const text = children
    const [isReadMore, setIsReadMore] = useState(true)
    const toggleReadMore = () => {
        setIsReadMore(!isReadMore)
    }

    return (
        <p className="text">
            {isReadMore ? text.slice(0, 150) : text}
            <span onClick={toggleReadMore} className="read-or-hide">
        {isReadMore ? "...lees meer" : " minder"}
      </span>
        </p>
    );
};

export default ReadMore