import * as React from 'react'
import Layout from '../components/layout.js'

const AboutPage = () => {
    return (
        <Layout pageTitle="About">
            Ik leer nu Gatsby maar dan ook een beetje React, dan heb ik straks vrijheid. Veel groetjes.
        </Layout>
    )
}

export default AboutPage