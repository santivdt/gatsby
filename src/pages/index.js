import * as React from 'react'
import Greeting from '../components/greetings.js'
import Layout from '../components/layout.js'
import { StaticImage } from "gatsby-plugin-image"

const IndexPage = () => {
  return (
      <Layout pageTitle="Home">
          <p>Hi i am making a Website</p>
          <Greeting name="Snooty"/>
          <StaticImage
              src="https://api.time.com/wp-content/uploads/2017/03/manatee-endangered-species.jpeg?w=800&quality=85"
              alt="Een plaatje van een zeekoe" />
      </Layout>
  )
}

export default IndexPage