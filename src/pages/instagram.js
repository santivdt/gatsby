import * as React from 'react'
import Layout from '../components/layout.js'
import Insta from '../components/instagram-feed/instagram'

const InstaPage = () => {
    return (
        <Layout pageTitle="Instagram">
            <Insta/>
        </Layout>
    )
}

export default InstaPage