import * as React from 'react'
import { graphql } from 'gatsby'
import Layout from "../components/layout";

const Wordpress = ({ data }) => {
    console.log(data)
    return (
        <Layout>

            <h4>Posts imported from Wordpress</h4>
            {data.allWpPost.edges.map(({ node }) => (
                <div key={node.id}>
                    <h2> {node.title} </h2>
                    <div dangerouslySetInnerHTML={{ __html: node.content }} />
                </div>
            ))}
        </Layout>
    )
}

export const query = graphql`
      query {
          allWpPost {
            edges {
              node {
                id
                title
                content
                slug
                status
                link
              }
            }
          }
    }
`

export default Wordpress